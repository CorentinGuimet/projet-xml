<%@ page import ="unice.miage.projetxml.DataBean" %>
<%@ page import="javax.xml.bind.JAXBContext" %>
<%@ page import="javax.xml.bind.Unmarshaller" %>
<%@ page import="org.inria.fr.ns.cr.Crs" %>
<%@ page import="java.io.File" %>
<%@ page import="javax.xml.bind.JAXBException" %>
<html>
   <head>
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
   	   <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   	   <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
   </head>
    <body>
    <div class="container">
        <h2 class="center-align">Application Raweb</h2>
      
        <div class="row">
          <div class="col s12 l12 m12">1</div>
          	<div class="card white">
            	<div class="card-content black-text">
              		<span class="card-title">Centres de recherche</span>    
              		<p>O� se trouvent les centres de recherche de l'Inria?</p>

					<%
						String markers = "";

						try {
							JAXBContext jc = JAXBContext.newInstance("org.inria.fr.ns.cr");
							Unmarshaller unmarsh = jc.createUnmarshaller();
							//unmarsh.setValidating(true);

							Crs crs = (Crs) unmarsh.unmarshal(new File("src/main/xml/centre.xml"));
							java.util.List<Crs.Cr> centres = crs.getCr();
							for(Crs.Cr centre : centres)
							{
							    markers += "markers=color:red%7C"+centre.getAdressegeographique().getLatitude()+","+centre.getAdressegeographique().getLongitude()+"&";
							}
						} catch (JAXBException e) {
							e.printStackTrace();
						}
						out.print("<img src=\"https://maps.googleapis.com/maps/api/staticmap?center=Bourges,France&zoom=5&size=600x300&maptype=roadmap&"+ markers +"key=AIzaSyAKlXAP9HHQui75J52Pg9dqD-UNH1jCKPs\">");
					%>
            	</div>
            	 <div class="card-tabs">
      				<ul class="tabs tabs-fixed-width">
        				<li class="tab"><a class="active" href="#centre1">Requete 1</a></li>
        				<li class="tab"><a  href="#centre2" >Requete 2</a></li>
        				<li class="tab"><a href="#centre3" >Requete 3</a></li>
      				</ul>
    			</div>
    			<div class="card-content black-text">
              		<div id="centre1">R�sultat r1</div>
					<div id="centre2">R�sultat r2</div>
					<div id="centre3">R�sultat r3</div>            	
            	</div>
          </div>
        </div>
        
        <div class="row">
        <div class="col s12 m12 l12">
            <div class="card white">
            	<div class="card-content black-text">
              		<span class="card-title">Statistiques</span>    
              		<p>Ici des statistiques de bases</p>  
         <% 
         DataBean db = new DataBean();
         out.print(db.query(db.getPShortName(),db.getContext()));
         out.print(db.test()); 
         
         
         %>
              		    	
            	</div>
            	 <div class="card-tabs">
      				<ul class="tabs tabs-fixed-width">
        				<li class="tab"><a class="active" href="#stat1">Stat 1</a></li>
        				<li class="tab"><a  href="#stat2">Stat 2</a></li>
        				<li class="tab"><a href="#stat3">Stat 3</a></li>
      				</ul>
    			</div>
    			<div class="card-content black-text">
              		<div id="stat1">Gaph1</div>
					<div id="stat2">Graph2</div>
					<div id="stat3">Graph3</div>            	
            	</div>
          </div>
        </div>
       </div> 
       
       
    </body>
</html>
