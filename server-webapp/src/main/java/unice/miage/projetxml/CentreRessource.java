package unice.miage.projetxml;

import org.inria.fr.ns.cr.Crs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/centres")
@Produces("application/xml")
public class CentreRessource {

    public CentreRessource() {
    }

    @GET
    public List<Crs.Cr> getCentres() {
        System.out.println("getCentres");

        return BD.getCentres();
    }

    @GET
    @Path("{id}")
    public Crs.Cr getCentre(@PathParam("id") String idCentre) {
        List<Crs.Cr> centres = BD.getCentres();

        for (Crs.Cr current : centres) {
            if (current.getNumnatstructrep().equals(idCentre)) {
                return current;
            }
        }
        return null;
    }


}
