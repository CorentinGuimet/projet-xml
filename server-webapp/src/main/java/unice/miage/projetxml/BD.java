package unice.miage.projetxml;

import org.inria.fr.ns.cr.Crs;
import org.inria.fr.ns.sr.StructureInria;

import java.util.ArrayList;
import java.util.List;

public class BD {

    private static List<StructureInria> lst_projets = new ArrayList<StructureInria>();
    private static  List<Crs.Cr> lst_centres = new ArrayList<Crs.Cr>();

    static {
        lst_projets.add(new StructureInria());
        lst_projets.add(new StructureInria());
        lst_projets.add(new StructureInria());
        lst_centres.add(new Crs.Cr()); // Renvoie une erreur HTTP 500
        lst_centres.add(new Crs.Cr());
        lst_centres.add(new Crs.Cr());
        
        // Faire des requetes XQuery ici poyr recuperer les donnees de la BD
        // new Structureinria(resRequeteXQuery)
    }

    public static List<StructureInria> getProjets() {
        return lst_projets;
    }
    public static List<Crs.Cr> getCentres() {
        return lst_centres;
    }
}
