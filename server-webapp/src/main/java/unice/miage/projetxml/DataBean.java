package unice.miage.projetxml;

import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.Set;
import org.basex.core.cmd.XQuery;

/**
 * 
 *
 *
 */
public class DataBean {
	
	private Context context;
	
	public DataBean() throws BaseXException {
		context = new Context();
		
		System.out.println(">>> Creation de la Base de Donnee Raweb <<<");

        // You can modify the CREATEFILTER property to import XML
        // files with suffixes other than XML (for example KML):
        new Set("CREATEFILTER", "*.xml").execute(context);


        // Create a collection and add all documents within the specified path
        new CreateDB("DB", "src/main/resources/Raweb/").execute(context);
        
        context.close();
		
	}
	
	public Context getContext() {
		return this.context;
	}
	
	/**
	 * Execute une requete XQuery
	 * @param query
	 * @param context
	 * @return
	 * @throws BaseXException
	 */
	public  String query(final String query, Context context) throws BaseXException {
	    
		  String req = (new XQuery(query).execute(context));
		  System.out.println(">>> Requete executee <<<");
	      return req;
	      
	  }
	
	/**
	 * Recuper le nom (shortname) de chaque projet
	 * @return la requete XQuery
	 */
	public  String getPShortName() {
		//System.out.println(">>> getPShortName() <<<");
		return "for $doc in collection('DB')"
				+ "let $file-path := base-uri($doc)"
				+ "where ends-with($file-path, '.xml')"
				+ "return concat('', $doc//raweb/identification/shortname)";
	}
	public String test() {return "test";}
	

}
