package unice.miage.projetxml;

import org.inria.fr.ns.sr.StructureInria;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/projets")
@Produces("application/xml")
public class ProjetRessource {

    public ProjetRessource() {
    }

    @GET
    public List<StructureInria> getProjets() {
        System.out.println("getProjets");

        return BD.getProjets();
    }

    @GET
    @Path("{id}")
    public StructureInria getProjet(@PathParam("id") String idProjet) {
        List<StructureInria> projets = BD.getProjets();

        for (StructureInria current : projets) {
            if (current.getSiidEquipeGroupe().equals(idProjet)) {
                return current;
            }
        }

        return null;
    }
}
