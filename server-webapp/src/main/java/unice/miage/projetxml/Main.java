package unice.miage.projetxml;

import org.basex.BaseXClient;
import org.basex.core.BaseXException;
import org.basex.core.Context;
import org.basex.core.cmd.Close;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.CreateIndex;
import org.basex.core.cmd.DropDB;
import org.basex.core.cmd.DropIndex;
import org.basex.core.cmd.InfoDB;
import org.basex.core.cmd.List;
import org.basex.core.cmd.Open;
import org.basex.core.cmd.Set;
import org.basex.core.cmd.XQuery;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.inria.fr.ns.cr.Crs;
import org.inria.fr.ns.sr.StructureInria;
import org.inria.fr.ns.sr.StructuresInria;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

public class Main {
    public static final URI BASE_URI = getBaseURI();

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/rest/").port(9992).build();
    }
    /** Database context. */
    //static Context context = new Context();

    
    public static void main(String[] args) throws IOException {
//        ResourceConfig config = new ResourceConfig();
//        config.registerClasses(CentreRessource.class, ProjetRessource.class);
//
//        try {
//            HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, config);
//            server.start();
//
//            System.out.println(String.format("Serveur lancé sur le port 9992"));
//
//            System.in.read();
//            server.shutdownNow();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

        /*
        try {
            JAXBContext jc = JAXBContext.newInstance("org.inria.fr.ns.sr");
            Unmarshaller unmarsh = jc.createUnmarshaller();
            //unmarsh.setValidating(true);

            StructuresInria structs = (StructuresInria) unmarsh.unmarshal(new File("src/main/xml/equipe.xml"));
            java.util.List<StructureInria> equipes = structs.getStructureinria();
            for(StructureInria equipe : equipes)
            {
                System.out.println(equipe.getLibellefr());
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }


        try {
            JAXBContext jc = JAXBContext.newInstance("org.inria.fr.ns.cr");
            Unmarshaller unmarsh = jc.createUnmarshaller();
            //unmarsh.setValidating(true);

            Crs crs = (Crs) unmarsh.unmarshal(new File("src/main/xml/centre.xml"));
            java.util.List<Crs.Cr> centres = crs.getCr();
            for(Crs.Cr centre : centres)
            {
                System.out.println(centre.getLibelle());
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        */


    
       

    	
    }	
   
    
}
