package unice.miage.projetxml.adaptateurs;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AdaptateurDateTime extends XmlAdapter<String, LocalDateTime>{

	/**
	 * Java vers XML
	 */
	@Override
	public String marshal(LocalDateTime v) throws Exception {
		
		return null;
	}

	/**
	 * XML vers Java
	 */
	@Override
	public LocalDateTime unmarshal(String v) throws Exception {
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDateTime res = LocalDateTime.parse(v, dateFormatter);
		
		return res;
	}

}
