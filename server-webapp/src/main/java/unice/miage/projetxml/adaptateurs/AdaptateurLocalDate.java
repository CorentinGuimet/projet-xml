package unice.miage.projetxml.adaptateurs;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AdaptateurLocalDate extends XmlAdapter<String, LocalDate>{

	@Override
	public String marshal(LocalDate v) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LocalDate unmarshal(String v) throws Exception {
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate res = LocalDate.parse(v, dateFormatter);
		
		return res;
	}

}
