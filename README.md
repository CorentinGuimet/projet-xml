Projet XML 2017-2017

Par :
S�bastien COTTE
Corentin GUIMET
Cassandra-Tiffany VILLERONCE
J�r�my JAGUT


# Pour lancer l'application :
1. Se placer dans le r�pertoire server-webapp du projet
2. mvn clean install
3. mvn -Djetty.http.port=9999 jetty:run
4. Ouvrir dans un navigateur : http://localhost:9999/server-webapp/

